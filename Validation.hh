<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\validation
{
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;
	/**
	 * 
	 * @package nushell\core
	 */
	<<singleton>>
	class Validation extends Plugin
	{
		public static function getInstance(/* HH_FIXME[4033] */...$args):Validation
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self();
		}

		public function __construct()
		{
			parent::__construct();
		//	$this->validation = new Validation();
		}


		//IF Numeric Validation
		public function isNum(?string $var):bool
		{
			return (is_numeric($var)) ? true : false;
		}

		//Length character Validation
		public function LenChr(?string $var, int $min=6):bool
		{
			return (strlen($var)>=$min) ? true : false;
		}

		//whitespace Validation
		public function WhiSpc(?string $var):bool
		{
			return (preg_match('/\s/',$var) == 1)? true :false;
		}

		//Email Format Validation
		public function isEmail(?string $email):bool
		{
			return (!filter_var($email, FILTER_VALIDATE_EMAIL))? false : true;
		}

		//URL Format Validation
		public function isURL(?string $url):bool
		{
			return (preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$url) == 1)? true : false;
		}

		//Match Both String 
		public function isMatch(?string $a, ?string $b):bool
		{
			if(is_null($a) && is_null($b))
			{
				return true;
			}
			elseif (is_null($a) && !is_null($b)) 
			{
				return false;
			}
			elseif (!is_null($a) && is_null($b))
			{
				return false;
			}
			elseif($a === $b)
			{
				return true;
			}
			else
			{
				return false;
			}
			//return ($a === $b)? true : flase;
		}
	}
}
